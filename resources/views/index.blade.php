@extends('layouts.default')
@section('content')
	<div class="container-fluid slider_cc p-0">
  <span id="ct-slider--prev">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <path d="M7.33 24l-2.83-2.829 9.339-9.175-9.339-9.167 2.83-2.829 12.17 11.996z"></path>
    </svg>
  </span>
  <span id="ct-slider--next">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <path d="M7.33 24l-2.83-2.829 9.339-9.175-9.339-9.167 2.83-2.829 12.17 11.996z"></path>
    </svg>
  </span>
  <div class="banner_slider_inner">
    <div class="banner_slider">
      <div class="banner_slider_top">
        <div class="banner_slider">
          <div class="banner_slider_single slide slide--1" >
          </div>
          <div class="overlay">
          </div>
          <div class="overlay-content">
              <h4>sha shib aviation academy</h4>
              <h2>Find Your</h2>
              <h2>Career</h2>
            <div class="btn_inner m2t-em anim">
              <a href="#" class="sim-button button16">Learn More</a>
            </div>
          </div>

        </div>
      </div>
    </div>
    <div class="banner_slider">
      <div class="banner_slider_top">
        <div class="banner_slider">
          <div class="banner_slider_single slide slide--1" >
          </div>
          <div class="overlay">
          </div>
          <div class="overlay-content">
              <h4>content2</h4>
          </div>
        </div>
      </div>
    </div>
    <div class="banner_slider">
      <div class="banner_slider_top">
        <div class="banner_slider">
          <div class="banner_slider_single slide slide--1" >
          </div>
          <div class="overlay">
          </div>
          <div class="overlay-content">
              <h4>content3</h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid p-0">
<div class="row justify-content-center align-self-center position-relative overflow-hidden m-0">
	<div class="border_img plane animation-element anoop">
		<img src="{{ asset('assets/saakochi/images/plane.png')}}" alt="">
	</div>
	<div class="border_img_bottom mark">
		<img src="{{ asset('assets/saakochi/images/mark.jpg')}}" alt="">
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12 col-12 p-0 align-self-center">
		<div class="sha_shib_group text-center h-100">
			<div class="_group_inner">
				<div class="_group_btn animation-element ">
					<img src="{{ asset('assets/saakochi/images/video.png')}}" alt="">
					<div class="_group_text">
						<h2>SHA-SHIB Group</h2>
						<h4>well-researched & Focused</h4>
						<h4>Course MateriAL</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12 col-12 p-0 align-self-center">
		<div class="sha_shib_academy ">
			<div class="welcome_text text-center animation-element">
				<div class="welcome_head m2b-em">
					<h2>Welcome To</h2>
					<h4>Sha-Shib Aviation Academy</h4>
				</div>
				<div class="welcome_content">
					<h5>About Sha-Shib Aviation Academy</h5>
					<p>Sha-Shib Aviation Academy, a premier institute of Sha-Shib Group of Institutions located at a prime area just opposite to Cochin International Airport, Kochi (Kerala) is wholly dedicated to develop Aeronautical skills & techniques to lead aviation maintenance industry and to impart ab-initio training for Aircraft Maintenance Engineering in compliance with Civil Aviation Requirements since June, 2007 under due approval of Director General of Civil Aviation, Ministry of Civil Aviation, Govt. of India, the regulatory authority for Civil Aviation in India.</p>
				</div>
				<div class="btn_inner m2t-em anim">
              <a href="{{url('about-sha-shib-aviation-academy#about')}}" class="sim-button button16">Learn More</a>
            </div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="container-fluid p-0 b_btm_blue bg_grey">
	<div class="container">
		<div class="row justify-content-center align-self-center">
			<div class="offer_courses">
				<div class="offer_courses_inner">
					<h3>Courses We Offer</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row justify-content-center align-self-center h-100 bot_serv_blk">
		<div class="col-lg-4 col-md-4 col-sm-12 col-12 justify-content-center align-self-center p-0">
			<div class="inner_course air_craft">
				<div class="inner_text">
					<div class="text_head">
						<h4>Aircraft Maintanance</h4>
						<h4>Engineering</h4>
					</div>
					<div class="course_con">
						<p>An Aircraft Maintenance Engineer (AME) is basically a person licensed to ensure the airworthiness of the aircraft before flying in accordance with the national & international aviation standards. In other words, an AME certifies the aircraft for its fitness before flying.</p>
					</div>
					<div class="btn_inner m2t-em">
						<a href="{{url('aircraft-maintenance-engineer#ame')}}" class="sim-button button16">Learn More</a>
					</div>
				</div>
			</div>
			<div class="overlay">
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 col-12 justify-content-center align-self-center p-0 mob_m1t-em">
			<div class="inner_course aeronotic_eng">
				<div class="inner_text">
					<div class="text_head">
						<h4>Airhostess & Hospitality Training</h4>
					</div>
					<div class="course_con">
						<p>The career profile of a cabin crew is very highly revered and has always been so. This is because the duties and responsibilities that the job entails are not only related to glamour, but carry a very high safety responsibility. The role of cabin crew is to provide excellent customer service to passengers while ensuring their comfort and safety throughout the flight.</p>
					</div>
					<div class="btn_inner m2t-em">
						<a href="{{url('airhostess-hospitality-training#aht')}}" class="sim-button button16">Learn More</a>
					</div>
				</div>
			</div>
			<div class="overlay">
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 col-12 justify-content-center align-self-center p-0 mob_m1t-em animation-element">
			<div class="inner_course pilot_training">
				<div class="inner_text">
					<div class="text_head">
						<h4>Pilot Training</h4>
					</div>
					<div class="course_con">
						<p>The demand for the pilot license holders is again growing stronger and the aspirants can now actually see their dreams flying to reality. IGIA, with its specialization in aviation and extensive experience in national and international pilot training is among a selected few in India and is proving to be a perfect launch pad for pilots with its International associations for the best possible Pilot training.</p>
					</div>
					<div class="btn_inner">
						<a href="{{url('pilot-training#pt')}}" class="sim-button button16">Learn More</a>
					</div>
				</div>
			</div>
			<div class="overlay">
			</div>
		</div>
	</div>
</div>

<div class="container-fluid p-0">
	<div class="row justify-content-center align-self-center h-100">
		<div class="col-md-6 justify-content-center align-self-center p-0">
			<div class=" bg_sha_grp">
				<div class="overlay">
				</div>
				<div class="sha_group_btm">
					<div class="sha_group_btm_inner">
						<h2>SHA-SHIB GROUPS</h2>
						<div class="sha__con">
							<p>SHA-SHIB Group is a progressive educational institution, dedicated to the pursuit of excellence. Students here are encouraged on this journey by our committed faculty, world-class facilities and student-friendly educational systems.Sha-shib Group – A leading house of education and a dedicated organization of number of institutes to provide wide range of Aviation Courses in India viz.</p>
						</div>
						<div class="sha__list">
							<ul>
								<li>Aircraft Maintenance Engineer (AME)</li>
								<li>Aeronautical Engineer</li>
								<li>Pilot Training</li>
							</ul>
						</div>
						<div class="sha__con">
							<p>It established its name in South Asia for aircraft maintenance engineering with help of excellent faculty and well equipped seven colleges in six states of India in 27 years of educational service in specific area.</p>
						</div>
						<div class="btn_inner m2t-em">
						<a href="{{url('enquiry#apply')}}" class="sim-button button16">Apply Now</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 justify-content-center align-self-center mtb-1em">
			<div class="fascilities mob_m1t-em">
				<div class="fas_top justify-content-between">
					<div class="fs_left">
						<h2>FACILITIES </h2>
					</div>
					<div class="fs_right">
						<a href="#">VIEW ALL</a>
					</div>
				</div>
				<div class="fascilities_list">
					<div class="fs_row">
						<div class="d-flex flex-row justify-content-center align-self-center bg_f5f3f4 fl_single">
							<div class="col-lg-6 col-md-6 fascilities_l_img p-0">
								<img src="{{ asset('assets/saakochi/images/hostel.jpg')}}" alt="">
							</div>
							<div class="col-lg-6 col-md-6 fascilities_right align-self-center">
								<h4>Hostel</h4>
								<a href="#">VIEW</a>
							</div>
						</div>
						<!-- /// -->
						<div class="flex-row-reverse d-flex justify-content-center align-self-center bg_f5f3f4 fl_single">
							<div class="col-lg-6 col-md-6 fascilities_l_img p-0">
								<img src="{{ asset('assets/saakochi/images/sports.jpg')}}" alt="">
							</div>
							<div class="col-lg-6 col-md-6 fascilities_right align-self-center text-right">
								<h4>Sports</h4>
								<a href="#">VIEW</a>
							</div>
						</div>
						<!-- /// -->
						<div class="d-flex flex-row justify-content-center align-self-center bg_f5f3f4 fl_single">
							<div class="col-lg-6 col-md-6 fascilities_l_img p-0">
								<img src="{{ asset('assets/saakochi/images/cultural-activities.jpg')}}" alt="">
							</div>
							<div class="col-lg-6 col-md-6 fascilities_right  align-self-center">
								<h4>Cultural Activities</h4>
								<a href="#">VIEW</a>
							</div>
						</div>
						<!-- /// -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection