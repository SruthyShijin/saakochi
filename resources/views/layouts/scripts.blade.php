<script src="{{ asset('assets/saakochi/js/jquery-2.2.0.min.js') }}"></script>
<script src="{{ asset('assets/saakochi/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/saakochi/js/slick.min.js') }}"></script>
<script src="{{ asset('assets/saakochi/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('assets/saakochi/js/index.js') }}"></script>
<script src="{{ asset('assets/saakochi/js/lightbox.js') }}"></script>
<script src="{{ asset('miniweb/assets/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript">

$(document).ready(function(){
  $('.dropdown-submenu a.test').on("click", function(e){
    $(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
  });



$('.approval-slide').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 4000,
            arrows: true,
            prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',
            
            responsive: [
            {
            breakpoint: 991,
            settings: {
                slidesToShow: 1,
                vertical:false,
            }
            },
            {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                vertical:false,
            }
            }
        ]
        });




 $(".content").mCustomScrollbar({theme:"dark-3"});

});

    jQuery('.banner_slider_inner').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 4000,
            arrows: true,
            prevArrow: $('#ct-slider--prev'),
            nextArrow: $('#ct-slider--next'),
            dots: true,
            infinite: true,
            cssEase: 'linear',
            responsive: [
            {
            breakpoint: 991,
            settings: {
                slidesToShow: 1,
                vertical:false,
            }
            },
            {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                vertical:false,
            }
            }
        ]
        });
$(document).ready(function(){
    $(".small_h .button a").click(function(){
        $(".overlay_h").fadeToggle(200);
       $(this).toggleClass('btn-open').toggleClass('btn-close');
    });
        $('.small_h .btn-close').on('click', function(){
            $(".overlay").fadeToggle(200);
        });

        // button animation
var $animation_elements = $('.animation-element');
var $window = $(window);

function check_if_in_view() {
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = (window_top_position + window_height);

  $.each($animation_elements, function() {
    var $element = $(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = (element_top_position + element_height);

    //check to see if this current container is within viewport
    if ((element_bottom_position >= window_top_position) &&
        (element_top_position <= window_bottom_position)) {
      $element.addClass('in-view');
    } else {
      $element.removeClass('in-view');
    }
  });
}

$window.on('scroll resize', check_if_in_view);
$window.trigger('scroll');

});

    </script>