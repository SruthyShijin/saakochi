
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sha-Shib Aviation Academy</title>
    
    <link rel="icon" href="{{ asset('assets/saakochi/images/fav.png') }}" type="image/png" sizes="16x16">
    <link href="{{ asset('assets/saakochi/_css/Icomoon/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/saakochi/_css/main.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('assets/saakochi/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/saakochi/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/saakochi/stackpath.bootstrapcdn.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/saakochi/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/saakochi/css/jquery.mCustomScrollbar.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/saakochi/css/lightbox.css') }}">


    