<!DOCTYPE html>
<html lang="en">

<head>
    
    @include('layouts.meta-data')
   @section('head')
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    @show
</head>

<body class="home">
    <div class="container header-nav">
        <nav class="navbar justify-content-between navbar-expand-lg top_nav_left">
            <a class="navbar-brand" href="index.php" target="_blank">
                <img src="{{ asset('assets/saakochi/images/logo.png')}}" class="d-inline-block align-top">
            </a>
      <ul class="navbar-nav top_nav_right">
                <li class="nav-item online_fee flex_disp">
          <div class="icon_left">
            <i class="fa fa-credit-card"></i>
          </div>
          <div class="con_inner">
            <a class="nav-link" target="_blank" href="#">Online Fee</a>
            <p>Make Payment</p>
          </div>
                </li>
        <li class="nav-item get_in_touch flex_disp">
          <div class="icon_left">
            <i class="fa fa-envelope"></i>
          </div>
          <div class="con_inner">
          <a class="nav-link" target="_blank" href="#">Get In Touch</a>
          <p>info@saakochi.com</p>
          </div>
                </li>
        <li class="nav-item call_now flex_disp">
          <div class="icon_left">
            <i class="fa fa-phone"></i>
          </div>
          <div class="con_inner">
          <a class="nav-link" target="_blank" href="#">Call Now</a>
          <p>+91 9594 929 936</p>
          </div>
                </li>
            </ul>
            <div class="small-menu small_h">

<nav>

<div class="button">
    <a class="btn-open" href="#"></a>
</div>
</nav>
<div class="overlay_h">
    <div class="wrap">
        <ul class="wrap-nav">
            <li><a href="#">Home</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Academics</a></li>
            <li><a href="#">Admission</a></li>
            <li><a href="#">Placement</a></li>
            <li><a href="#">Gallery</a></li>
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Apply Now</a></li>
        </ul>
    </div>
</div>
            </div>
        </nav>
  </div>
    <div class="bottom_top_nav">
        <nav class="navbar pos_absulute navbar-expand-md navbar-new-bottom">
            <div class="navbar-collapse collapse pt-2 pt-md-0" id="navbar1">
                   @widget('HeaderMenu') 
            </div>
        </nav>
    </div>
    @yield('content')
    <footer class="footer_sha_shib">
    <div class="container">
        <div class="row footer_align">
            <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                <div class="f_inner">
                    <h5>SHA-SHIB GROUPS</h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <div class="ss_icons">
                        <div class="ss_icon_single facebook">
                            <a href="#"><i class="fa fa-facebook-f"></i></a>
                        </div>
                        <div class="ss_icon_single twitter">
                            <a href="#"><i class="fa fa-twitter"></i></a>
                        </div>
                        <div class="ss_icon_single instagram">
                            <a href="#"><i class="fa fa-instagram"></i></a>
                        </div>
                        <div class="ss_icon_single gplus">
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                        </div>
                        <div class="ss_icon_single skype">
                            <a href="#"><i class="fa fa-skype"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                <div class="_our_services">
                    <h5>Our Services</h5>
                    <div class="_our_service_con">
                        <p>Academic</p>
                        <p>Planning & administration</p>
                        <p>Campus Safety</p>
                        <p>Office of the Chancellor</p>
                        <p>Fascility Services</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-12 mob_m1t-em">
                <h5>Academics</h5>
                <div class="_our_academics">
                    <p>Academic</p>
                    <p>Planning & administration</p>
                    <p>Campus Safety</p>
                    <p>Office of the Chancellor</p>
                    <p>Fascility Services</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-12 mob_m1t-em">
                <h5>Clients</h5>
                <div class="_our_clients">
                    <div class="cl_img">
                        <img src="{{ asset('assets/saakochi/images/cl1.jpg')}}" alt="">
                    </div>
                    <div class="cl_img">
                        <img src="{{ asset('assets/saakochi/images/cl2.jpg')}}" alt="">
                    </div>
                </div>
                <div class="_our_clients">
                    <div class="cl_img">
                        <img src="{{ asset('assets/saakochi/images/cl3.jpg')}}" alt="">
                    </div>
                    <div class="cl_img">
                        <img src="{{ asset('assets/saakochi/images/cl4.jpg')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright container">
        <div class="copyright_text text-center">
            <p>SSAA 2019 . All Right Reserved | Developed By : Spiderworks</p>
        </div>
    </div>
</footer>
</body>
 @include('layouts.scripts')

@section('bottom')

    @show
</html>
