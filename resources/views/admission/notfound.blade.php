@extends('layouts.inner')
@section('head')
  <style type="text/css">
    #box {
        padding: 20px;
        box-shadow: inset 0 -3em 3em rgba(0, 0, 0, 0.1), 0 0 0 2px rgb(255, 255, 255), 0.3em 0.3em 1em rgba(0, 0, 0, 0.3);
    }
</style>
    @show
@section('content')

   
<div class="main-sec abou-sec-1">
        <div class="container" class="col-md-12"> 
        <div id="box" class="col-lg" align="center">
              <h1>404</h1>
              <h6>Page not found</h6>
              <p>Sorry, the page you are looking for could not be found.</p>
            </div>
             </div><br>
  <div class="col-lg" align="center">
    <a href="{{url()->previous()}}"><div class="btn btn-primary" align="center">
              backto page
            </div></a>
  </div>
    </div>

            &nbsp;
                        &nbsp;

             
        
     
@endsection