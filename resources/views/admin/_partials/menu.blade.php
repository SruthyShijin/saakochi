@foreach($items as $key=>$item)

	<li>
		<a href="{{url($item->slug)}}"  class="detailed">
			<span class="title">{{$item->title}}</span>
			@if(isset($item->children))
				<span class="arrow"></span>
			@endif
		</a>
		<span class="icon-thumbnail"><i class="pg-calender"></i></span>
		@if(isset($item->children))
			<ul class="sub-menu">
		    	@include('admin._partials.menu', ['items'=>$item->children])
		    </ul>
		@endif
	</li>

@endforeach
