<ul class="navbar-nav w-100 justify-content-center px-3">
	@include('_partials.menu', ['items'=>$menu_items, 'depth'=>0])
</ul>