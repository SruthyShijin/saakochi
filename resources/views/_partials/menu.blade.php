@foreach($items as $key=>$item)
	<li @if($depth == 0) class="nav-item  @if(isset($item->children)) dropdown @endif" @else class="dropdown-submenu" @endif>
        <a @if($depth == 0) class="nav-link @if(isset($item->children)) dropdown-toggle @endif" @else class="dropdown-item @if(isset($item->children)) dropdown-toggle @endif" @endif href="{{$item->url}}" >{{$item->title}}</a>
        @if(isset($item->children))
			<ul class="dropdown-menu">
		    	@include('_partials.menu', ['items'=>$item->children, 'depth'=>$depth+1])
		    </ul>
		@endif
    </li>
@endforeach