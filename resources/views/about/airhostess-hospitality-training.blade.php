@extends('layouts.default')
@section('content')

<div class="container-fluid slider_cc p-0">
        <div class="banner_slider_inner">
          <div class="banner_slider">
            <div class="banner_slider_top">
              <div class="banner_slider">
                <div class=" slide slide--1" >
                        <img src="{{asset('assets/saakochi/images/offers.jpg')}}" class="img-fluid">

                </div>
                <div class="overlay">
                </div>
                <div class="overlay-content inner-page">
                    <h2>Airhostess & Hospitality<br/>Training</h2> 

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="main-sec abou-sec-1">
          <div class="container">



      <div class="row " id="aht">

          <div class="col-lg-9 col-md-9 ">

              <p>
         The career profile of a cabin crew is very highly revered and has always been so. This is because the duties and responsibilities that the job entails are not only related to glamour, but carry a very high safety responsibility. The role of cabin crew is to provide excellent customer service to passengers while ensuring their comfort and safety throughout the flight.</p>

      <p>During their career, cabin crew have the privilege of visiting new destinations and expanding their horizon. Airline crew have the opportunity to explore the world, learn new languages, visit countries never heard of before and expand their horizon, knowledge and skills all along on duty and living in the lap of luxury, overnight stays at different destinations are spent in the best hotels of the country has to offer along with access to the hotels amenities at the airlines expense or a discounted rate.</p>

        <p>Airlines also offer the opportunity to live and work from different base stations, crew members can apply for transfer to any destination that the airline has a base center in and settle to operate from there. This is just one of the benefits that crew can avail of , apart from the high flying ,globe trotting lifestyle crew receive various other off the job benefits as well. These include holidays abroad with free/discounted tickets and 5 star hotel stay at discounted rates. Certain airlines go further to ensure their crew is well taken care of, by providing discounts on purchases at base city stores and free accommodation with no over head arrears if employed in certain international airlines.</p>


          </div>

        <div class="col-lg-3 col-md-3 ">

           <h3><i class="fa fa-plane clr-prim" aria-hidden="true"></i>  COURSE OFFERED: </h3>
           <p><a>Aircraft Maintenance Engineer</a></p>
           <p><a>Aeronautical Engineering</a></p>
           <p><a>Pilot Training</a></p>




                <h3><i class="fa fa-plane clr-prim" aria-hidden="true"></i>Academics:</h3>


            <p><a> Course Structure</a></p>
            <p><a> Syllabus</a></p>
            <p><a> Terms And Conditions</a></p>

           <h3><i class="fa fa-plane clr-prim" aria-hidden="true"></i> Admission:</h3>


           <p><a> Admission Procedure </a></p>
           <p><a> Terms And Conditions</a></p>
           <p><a> Apply Online </a></p>

         </div>

      </div>









      </div>
      </div>



      <div class="main-sec p-0">

          <div class="row m-0">
            <div class="col-lg-6 col-md-6 abou-sec-7">
              <div class="container">
              <p><b>Facilities Provided : </b>Fully equipped and well ventilated classrooms with audio/visual learning facility. cabin familiarization/inflight service training will be conducted on aircrafts which are privately owned by the academy.</p>
              <p><b>Faculty : </b>  Experienced trainers with flying experience in international and domestic airlines.Fully equipped and well ventilated classrooms with audio/visual learning facility. cabin familiarization/inflight service training will be conducted on aircrafts which are privately owned by the academy.</p>
              <p><b>Attendance : </b>  80% attendance is compulsory to ensure through training and competence in every area of the chosen course.</p>
              
              <p><b>Assessment : </b> Students are assessed regularly to ensure they have grasped the subject. This is done to prepare the student for the recruitment by an airline were they will have to clear mandatory assessment – if recruited in India- or any other mandatory assessment – if recruited internationally before being cleared to fly.</p>
              <p>This course is offered at QUETZAL AIR HOSTESS AND HOSPITALITY ACADEMY (QAHA),Mummbai.</p>

                      


             



                



      








            </div>

            </div>

             <div class="col-lg-6 col-md-6 abou-sec-8">
              <div class="container">
              <h1>ELIGIBILITY CRITERIA</h1>
              <p> <i class="fa fa-plane" aria-hidden="true"></i>Age : 17 Yrs and above</p>
              <p><i class="fa fa-plane" aria-hidden="true"></i> Education Qualification: 10+2 pass. Any stream</p>
              <p><i class="fa fa-plane" aria-hidden="true"></i>Physical Fitness: Class 2 medical cleared by a D.G.C.A approved doctor</p>
              <p><i class="fa fa-plane" aria-hidden="true"></i>Minimum Height: 5.2 average for girls – 5.5 average for boys</p>

            </div>



          </div>
      </div>
      </div>



@endsection
