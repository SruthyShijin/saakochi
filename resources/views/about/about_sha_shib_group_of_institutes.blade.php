@extends('layouts.default')
@section('content')

<div class="container-fluid slider_cc p-0">

  <div class="banner_slider_inner">
    <div class="banner_slider">
      <div class="banner_slider_top">
        <div class="banner_slider">
          <div class=" slide slide--1" >
            <img src="{{asset('assets/saakochi/images/over-banner.jpg')}}" class="img-fluid">

          </div>
          <div class="overlay">
          </div>
          <div class="overlay-content inner-page">

              <h2>Sha Shib Group Of Institutions</h2>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="main-sec abou-sec-1">
	<div class="container">
<div class="row ">

	<div class="col-lg-6 col-md-6 ">
		<h1>ABOUT SHA-SHIB AVIATION ACADEMY</h1>
    <div class="content" style="height: 540px;" id="about">
		<p>
    Sha-Shib Aviation Academy, a premier institute of Sha-Shib Group of Institutions located at a prime area just opposite to Cochin International Airport, Kochi (Kerala) is wholly dedicated to develop Aeronautical skills & techniques to lead aviation maintenance industry and to impart ab-initio training for Aircraft Maintenance Engineering in compliance with Civil Aviation Requirements since June, 2007 under due approval of Director General of Civil Aviation, Ministry of Civil Aviation, Govt. of India, the regulatory authority for Civil Aviation in India.</p>
    <p>
    The civil aviation industry in India has emerged as one of the fastest growing industries in the country during the last three years. India is currently considered the third largest domestic civil aviation market in the world, to meet the skill & manpower demand of the Indian as well as world aviation industry the regulatory requirements have changed to harmonize with International Civil Aviation.
    </p>
    <p>
     The institute has also been approved as per the latest regulation i.e. CAR- 147( Basic) by Director General of Civil Aviation, Ministry of Civil Aviation, Govt. of India. This CAR is issued under Rule 133B of Aircraft Rule 1937 derived from Aircraft Act 1934(All to 1934) passed by Indian parliament.
    </p>
    <p>
    Aircraft Maintenance Engineering license is essential for certifying airworthiness of civil aircraft all over the world and is valid in 191 International Civil Aviation Organization (ICAO) member countries. Students can also seek to abroad for further studies.
    </p>
    <p>
    SHA-SHIB AVIATON ACADEMY. Kochi is pioneer and explorer in the field of Aviation Technology and having outstanding performance in national and International cadre. Students after completing Aircraft Maintenance Engineering course and obtaining the DGCA license have made us proud by getting recruited in Various International Airlines like Gulf Air, Qatar Airways, Etihad Airways, Emirates Airline, Trans Maldivian Airways, Oman Air, British Airways, Singapore Airlines etc.& Indian operators like Air India, Go Air, Indigo, Spice Jet, Jet Airways, Air Asia, Vistara Airlines etc.
    </p>
  </div>
	</div>
	<div class="col-lg-6 col-md-6 ">
            <img src="{{asset('assets/saakochi/images/about-1.jpg')}}" class="img-fluid">

	</div>
</div>
</div>
</div>

<div class="main-sec abou-sec-2">
  <div class="container">
<div class="row ">

   <div class="col-lg-4 col-md-4 ">
        <img src="{{asset('assets/saakochi/images/about-2.jpg')}}" class="img-fluid">

  </div>

  <div class="col-lg-8 col-md-8 ">
    <h1>Director's Desk</h1>
    <p>The prime concern was to empower young boys & girls joining our institutions, with creativity to meet the challenges of the 21st century. Over the years, our institutions have excelled in providing state of the art infrastructure, equipments and dedicated experienced faculties with commendable background. In our opinion, education should always develop human qualities in the students and inspire them to gain big achievements as well as develops qualities of exploration. We are determined to streamline all our resources with full dedication to translate into reality this great vision of education. We are confident and proud that our institutions with enviable hi-tech infrastructure can enable our student stand tougher in the challenging world outside.We whole heartedly welcome our students and wish them a prosperous and glorious future.</p>
  </div>
</div>
</div>
</div>
<div class="main-sec abou-sec-3">
  <div class="container">
<div class="row ">
  <div class="col-lg-8 col-md-8 ">
    <h1>Chief Instructor's Desk</h1>
    <p><u><i>“Ability is what you are capable of doing, Your Attitude determines how well you do it”</i></u></p>

<p>It gives me enormous pleasure to extend to you all a very warmed well come on behalf of Sha-Shib Aviation Academy.</p>

<p>It is a destination where we salute youth, excellence and commence towards transforming the students into a perfectly skilled and sophisticated Aircraft Maintenance Engineers.</p>

<p>It is the Aircraft Maintenance Engineer’s only who can certify the aircraft-“Fit to Fly”. In recent days the Aviation Industry is on its peak, plenty of scheduled and non-schedule operator’s are on queue to commence operation and many existing Airlines are planning to enhance their fleet. Hence the demand for licensed Aircraft Maintenance Engineer’s is huge. In Sha-Shib Aviation Academy we have highly qualified faculty members who guides the aspiring students and directs them to reach towards their goal.</p>

  </div>
     <div class="col-lg-4 col-md-4 ">
      <div class="img-cntr">
            <img src="{{asset('assets/saakochi/images/about-3.jpg')}}" class="img-fluid">

  </div>
</div>
</div>
</div>
</div>
<div class="main-sec abou-sec-4">
  <div class="container">
    <div class="row ">
      <div class="col-lg-7 col-md-7 ">
        <h1>Why Choose Sha-Shib?</h1>
        <p> Here are the advantages which makes Sha-Shib as one of the most preferred organization for Aircraft Maintenance Engineering/Aeronautical engineering aspirants in India.It has The best AME Colleges in India. </p>
        <ul>
          <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Centralized Academic monitoring by a team of Highly Academic expertise</li>
          <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Competitive professional workshops and regularly monitored by the Centralized Team of Highly Qualified and Experienced Engineers</li>
          <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Transportation facility for students at all centers</li>
          <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Hygienic and safe cafeterias at all centre</li>
          <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Library with a wide range of study and research material accessible.</li>
          <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Sports facilities</li>
          <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> 24/7 students surveillance</li>
          <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> 24/7 helpline for students.</li>
          <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Hostel & Mess facilities assistance at all centers</li>
          <li><i class="fa fa-angle-double-right" aria-hidden="true"></i> Well-equipped laboratories, coupled up with the latest learning infrastructure.</li>
        </ul>
      </div>
    </div>
</div>
</div>
<div class="main-sec p-0">

    <div class="row m-0">
      <div class="col-lg-6 col-md-6 abou-sec-5">
        <div class="container">
        <h1>Few Words For Students</h1>
        <p> As compared to other courses the students need a well organized planning for studies. Students should be well disciplined, dedicated and must maintain punctuality towards their studies. There is no short cut to success. We are not at all interested to complete the course as soon as possible but we rely on better understanding with the students, so as to get better and easy success in their examination. "Student's success is our motive" </p>
      </div>

      </div>

       <div class="col-lg-6 col-md-6 abou-sec-6">
        <div class="container">
        <h1>Few Words For Parents</h1>
        <p> Parents guidance is as much as importance as Parents. Parents are better equipped with the career of their children. We can foster the inner potential of the candidate & can put him/her on the right track where in the candidate besides acquiring Technical & Non Technical knowledge shall as well develop a moral & intellectual personality. As such parent should be in constant touch with their wards by coming to the college at times to find progress made or failure encountered by them. Parents and our experienced faculty members can jointly nurture them towards their excellence in career.</p>

      </div>
    </div>
</div>
</div>

@endsection
