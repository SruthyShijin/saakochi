@extends('layouts.default')
@section('content')

<div class="container-fluid slider_cc p-0">
        <div class="banner_slider_inner">
          <div class="banner_slider">
            <div class="banner_slider_top">
              <div class="banner_slider">
                <div class=" slide slide--1" >
                        <img src="{{asset('assets/saakochi/images/offers.jpg')}}" class="img-fluid">

                </div>
                <div class="overlay">
                </div>
                <div class="overlay-content inner-page">
                    <h2>Pilot Training</h2> 

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="main-sec abou-sec-1">
          <div class="container">



      <div class="row " id="pt">

          <div class="col-lg-9 col-md-9 ">

              <p>
         The demand for the pilot license holders is again growing stronger and the aspirants can now actually see their dreams flying to reality. IGIA, with its specialization in aviation and extensive experience in national and international pilot training is among a selected few in India and is proving to be a perfect launch pad for pilots with its International associations for the best possible Pilot training. The job of a pilot is a highly specialized one. It requires knowledge of air navigation, interpretation of meteorological reports, operating sophisticated electronic and mechanical controls, leading the aircraft under adverse circumstances, and being a leader to the flight crew and passengers under climatic and other emergency situations.</p>

      <p>Program Details</p>
      <ul>
          <li>Students Pilot Licence (SPL).</li>
          <li>Private Pilot Licence (PPL) 40 hours.</li>
          <li>Commercial Pilot Licence (CPL) 200 hours.</li>
          <li>50 hrs. Package.</li>
          <li>25 hrs. Package.</li>
          <li>Multi-Engine Rating (MER) 10 hours (on student request).</li>
          <li>Instrument Rating.</li>
          <li>Asst. Flight Instrument Rating.</li>
          <li>DGCA approved Pilot ground training course.</li>
          <li>Skill test by DGCA approved examiner.</li>
        </ul>

        <p>Eligibility Criteria</p>
           <ul>
          <li>Passed 10 +2 in science subjects (PCM) or higher qualification in science from recognized Board/University or
          </li>
          <li>Three years Diploma in any branch of Engineering.</li>
        </ul>

          </div>

        <div class="col-lg-3 col-md-3 ">

           <h3><i class="fa fa-plane clr-prim" aria-hidden="true"></i>  COURSE OFFERED: </h3>
           <p><a>Aircraft Maintenance Engineer</a></p>
           <p><a>Aeronautical Engineering</a></p>
           <p><a>Pilot Training</a></p>




                <h3><i class="fa fa-plane clr-prim" aria-hidden="true"></i>Academics:</h3>


            <p><a> Course Structure</a></p>
            <p><a> Syllabus</a></p>

           <h3><i class="fa fa-plane clr-prim" aria-hidden="true"></i> Admission</h3>


           <p><a> Admission Procedure </a></p>
           <p><a> Terms And Conditions</a></p>
           <p><a> Apply Online </a></p>

         </div>

      </div>









      </div>
      </div>






@endsection
