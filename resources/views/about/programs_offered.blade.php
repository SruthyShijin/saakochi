@extends('layouts.default')
@section('content')

<div class="container-fluid slider_cc p-0">
        <div class="banner_slider_inner">
          <div class="banner_slider">
            <div class="banner_slider_top">
              <div class="banner_slider">
                <div class=" slide slide--1" >
                        <img src="{{asset('assets/saakochi/images/offers.jpg')}}" class="img-fluid">

                </div>
                <div class="overlay">
                </div>
                <div class="overlay-content inner-page">
                    <h2>Aircraft Maintenance <br/>Engineer</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="main-sec abou-sec-1">
          <div class="container">



      <div class="row " id="ame">

          <div class="col-lg-9 col-md-9 ">

              <p>
         An Aircraft Maintenance Engineer (AME) is basically a person licensed to ensure the airworthiness of the aircraft before flying in accordance with the national & international aviation standards. In other words, an AME certifies the aircraft for its fitness before flying. Various Airlines, Maintenance & repair organizations, flying clubs, etc. appoint these AMEs for the safe flying of their aircrafts. In terms of employment opportunities, the aircraft maintenance engineering is probably the most promising in the aviation sector.</p>

      <p>
          The duty of an AME is to ensure that the Aircraft is in a fit to fly condition before it takes off the ground. No aircraft can take off without clearance certificate from qualified Maintenance Engineers. To become an AME one has to undergo the three years AME course from a DGCA approved Institute.</p>

        <p>  These recognised institutes teach the approved syllabus of DGCA & conduct internal examinations for completion of the course. Students who successfully complete the course can then appear for DGCA licence examinations i.e. Basic Aircraft Maintenance Engineering Licence (BAMEL) The Students 10+2 students from science stream with Physics, Chemistry & Mathematics, or any higher qualification with PCM or equivalent. As per DGCA (Govt. of India) rules only 60 seats are allotted per stream (Mechanical / Avionics). No AME institutes can induct more students than that allotted by DGCA.</p>

        <p>  Aspiring Aircraft Maintenance Engineers who have obtained the licence can start working with private/ government airlines. So one who has those goals in mind to become part of the challenging aviation industry, can join AME course & explore the world of his/her dreams.</p>

         <p> “SHA-SHIB GROUP OF INSTITUTIONS” is the largest group in South Asia for Imparting training in the field of Aircraft Maintenance Engineering, focusing on the comprehensive development of students and bring them up to face the challenges of life with courage and enable them to transform dreams into reality.</p>

         <p> THE Group offers Aircraft Maintenance Engineering course in many cities across India Viz. Bhopal, Guna, Pune, Mumbai, Bangalore, Bhubaneswar and Cochin, all these institutes are approved by Director General of Civil Aviation, Ministry of Civil Aviation, Govt. of India. The Institutes under Sha-Shib Group impart training in two streams i.e. Mechanical (Heavy Aeroplane, Jet Engine, Light Aeroplane, Piston Engine) as well as Avionics (Electrical system, Instrument system & Radio Navigation system of Aircraft.). The Students, who dream to be a successful AME & ultimately achieve their goal, may join these training Institutes. Like other Institutes/colleges under various regulatory bodies viz. AICTE, NCTE etc., these DGCA approved AME Institutes are also providing the necessary knowledge, experience and skill of Aircraft maintenance Technique in order to pass AME license.</p>

         <p> Merely completion of AME course will not fetch the students any license or degree/diploma. However those AME students who have successfully completed the AME course and passed AME license Examinations, conducted by DGCA will be eligible for Appropriate AME license.</p>


          </div>

        <div class="col-lg-3 col-md-3 ">

           <h3><i class="fa fa-plane clr-prim" aria-hidden="true"></i>  COURSE OFFERED: </h3>
           <p><a>Aircraft Maintenance Engineer</a></p>
           <p><a>Aeronautical Engineering</a></p>
           <p><a>Pilot Training</a></p>




                <h3><i class="fa fa-plane clr-prim" aria-hidden="true"></i>  Academics:</h3>


            <p><a> Course Structure</a></p>
            <p><a> Duration</a></p>
            <p><a> Course Fees with Break Up</a></p>
            <p><a> Syllabus</a></p>
            <p><a> Pratical Training</a></p>
            <p><a> Training Facilities</a></p>
            <p><a> Terms And Conditions</a></p>

           <h3><i class="fa fa-plane clr-prim" aria-hidden="true"></i>  Admission:</h3>


           <p><a> Admission Procedure </a></p>
           <p><a> Fee & Payment </a></p>
           <p><a> E-Prospectus </a></p>
           <p><a> E-Broacher </a></p>
           <p><a> Apply Online </a></p>

         </div>

      </div>









      </div>
      </div>



      <div class="main-sec p-0">

          <div class="row m-0">
            <div class="col-lg-6 col-md-6 abou-sec-7">
              <div class="container">
              <h1>COURSE OFFERED:</h1>
              <p> 1. SHA-SHIB AVIATION ACADEMY OFFERS 3 YEARS (FULL TIME) AIRCRAFT MAINTENANCE ENGINEERING COURSE APPROVED BY DIRECTOR GENERAL OF CIVIL AVIATION, GOVT. OF INDIA </p>
              <p><b>MECHANICAL STREAM (60 SEATS).</b></p>

              <div class="row">
                <div class="col-lg-6 col-md-6 ">
                  <div class="offer">
                    <i class="fa fa-plane" aria-hidden="true"></i>
                    <p>Light Aircraft (LA)</p>
                  </div>
                </div>

                 <div class="col-lg-6 col-md-6 ">
                  <div class="offer">
                    <i class="fa fa-plane" aria-hidden="true"></i>
                    <p>Light Aircraft (LA)</p>
                  </div>
                </div>
              </div>


                      <div class="row">
                <div class="col-lg-6 col-md-6 ">
                  <div class="offer">
                    <i class="fa fa-plane" aria-hidden="true"></i>
                    <p>Light Aircraft (LA)</p>
                  </div>
                </div>

                 <div class="col-lg-6 col-md-6 ">
                  <div class="offer">
                    <i class="fa fa-plane" aria-hidden="true"></i>
                    <p>Light Aircraft (LA)</p>
                  </div>
                </div>
              </div>


              <p><b> AVIONICS STREAM (60 SEATS) </b></p>


                      <div class="row">
                <div class="col-lg-6 col-md-6 ">
                  <div class="offer">
                    <i class="fa fa-plane" aria-hidden="true"></i>
                    <p>Light Aircraft (LA)</p>
                  </div>
                </div>

                 <div class="col-lg-6 col-md-6 ">
                  <div class="offer">
                    <i class="fa fa-plane" aria-hidden="true"></i>
                    <p>Light Aircraft (LA)</p>
                  </div>
                </div>
              </div>


                 <div class="row">
                <div class="col-lg-6 col-md-6 ">
                  <div class="offer">
                    <i class="fa fa-plane" aria-hidden="true"></i>
                    <p>Light Aircraft (LA)</p>
                  </div>
                </div>


              </div>




      <p> 2. ALSO OFFER 3 YEARS B.SC. IN AIRCRAFT MAINTENANCE SCIENCE FROM BHARATHIAR UNIVERSITY, COIMBATORE UNDER CPP MODE </p>








            </div>

            </div>

             <div class="col-lg-6 col-md-6 abou-sec-8">
              <div class="container">
              <h1>ELIGIBILITY CRITERIA</h1>
              <p> <i class="fa fa-plane" aria-hidden="true"></i> Passed 10 +2 in science subjects (PCM) or a higher qualification in science from recognized Board/University.or</p>
              <p><i class="fa fa-plane" aria-hidden="true"></i> Three years Diploma in any branch of Engineering.</p>
              <p><b>FEE & PAYMENT</b></p>

              <div class="eligibile">
                <a class="btn">Apply</a>
                <p>6,50,000 INR for 3 Years.</p>
                <div class="clearfix"></div>
              </div>

            </div>



          </div>
      </div>
      </div>



@endsection
