@extends('layouts.default')

@section('content')

<div class="container-fluid slider_cc p-0">

  <div class="banner_slider_inner">
    <div class="banner_slider">
      <div class="banner_slider_top">
        <div class="banner_slider">
          <div class=" slide slide--1" >
            <img src="{{asset('assets/saakochi/images/over-banner.jpg')}}" class="img-fluid">
          </div>
          <div class="overlay">
          </div>
          <div class="overlay-content inner-page">

              <h2>Lorem Ipsum is simply dummy
text of the printing and typesetting industry.</h2>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="main-sec mis-bg">
	<div class="container">
<div class="row ">

	<div class="col-lg-9 col-md-10 col-sm-12 col-12">
		<h1>Mission / Vision</h1>
		<p>To emerge as a world – class academy in creating and disseminating knowledge and providing students a unique learning as well as fully hands on practical experience on latest fleets. Our philosophy is to shape up good and skillful aircraft engineers. Our students are made to face the industry head on by way of numerous industrial projects which expose them to the state-of-the art technology. We boast the highest on campus recruitment and a vast majority of our engineers are accepted by reputed aviators. We have tie-ups with various industries and educational institutions for transfer of technology. It is therefore not surprising that we get students from all parts of India. Our principal asset is the highly qualified faculty members, majority of them with international exposure and our fully equipped laboratories. Students and guardians are encouraged to have continuous interaction with the faculty for a seamless understanding of the student’s progress. We give a keen emphasis on individual student counseling for their overall development that will best serve the world and betterment of mankind..</p>
	</div>
</div>
</div>
</div>
<div class="main-sec why">
	<div class="container">

		<div class="row ">
			<div class="col-lg-12 col-md-12 col-sm-12 col-12">
				<h1>Why US</h1>
			</div>
		</div>

		<div class="row ">
			<div class="col-lg-12 col-md-12 col-sm-12 col-12">
<div class="accordion" id="accordionExample">
  <div class="why-cntr">
    <div class="" id="headingOne">
      <h2 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          <i class="fa fa-plus" aria-hidden="true"></i> <span>01</span>
        </button>
      </h2>
    </div>
    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="row pt-5 pb-5">
        <div class="col-md-6 text-right">
            <img src="{{asset('assets/saakochi/images/over-img-3.jpg')}}" class="img-fluid">
        </div>
        <div class="col-md-6">
        	<div class="num">01</div>
        	<div class="clearfix"></div>
        	<p>
        		Sha-Shib Aviation Academy, Kochi is one of the Best & progressive institute under the banner of SHA-SHIB GROUP which has earned its name as The Largest Group in South Asia for imparting training in the field of Aircraft Maintenance Engineering with 29 glorious years of excellence in aviation industry.
        	</p>
        </div>
      </div>
    </div>
  </div>
  <div class="why-cntr">
    <div class="" id="headingTwo">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          <i class="fa fa-plus" aria-hidden="true"></i> <span>02</span>
        </button>
      </h2>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="row pt-5 pb-5">

        <div class="col-md-6">
        	<div class="num num2">02</div>
        	<div class="clearfix"></div>
        	<p class="text-allign-right">
                    SAA is approved in Mechanical stream[B1.1] Avionics stream[B2] by Director General of Civil Aviation, Ministry of Civil Aviation, Govt. of India under CAR Section-2,Series E, Part-VIII CAR-147 to harmonize training requirements of maintenance training organizations to international standards.
        	</p>
        </div>
          <div class="col-md-6 text-left">
            <img src="{{asset('assets/saakochi/images/AME.png')}}" class="img-fluid">

        </div>
      </div>
    </div>
  </div>
  <div class="why-cntr">
    <div class="" id="headingThree">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
           <i class="fa fa-plus" aria-hidden="true"></i> <span>03</span>
        </button>
      </h2>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
     <div class="row pt-5 pb-5">
        <div class="col-md-6 text-right">
                <img src="{{asset('assets/saakochi/images/SAA.png')}}" class="img-fluid">

        </div>
        <div class="col-md-6">
        	<div class="num">03</div>
        	<div class="clearfix"></div>
        	<p>
                    SAA is situated on very prime location just opposite to Cochin airport, Indias fourth-largest international airport, has become the first airport to function completely on solar energy, marking a major milestone on our planet. It fuels our students to fulfill their dream.

        	</p>
        </div>
      </div>
    </div>
  </div>

  <div class="why-cntr">
    <div class="" id="headingFour">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
           <i class="fa fa-plus" aria-hidden="true"></i> <span>04</span>
        </button>
      </h2>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
     <div class="row pt-5 pb-5">
     <div class="col-md-6">
    <div class="num num2">04</div>
    <div class="clearfix"></div>
    <p class="text-allign-right">
        Our aircraft hangar is filled with plenty of synthetic working mock-ups along with fully running Lear-Jet 25B and Cessna-150 aircrafts for better understanding exposure of practical knowledge to students.</p>
        </div>
        <div class="col-md-6 text-left">
                <img src="{{asset('assets/saakochi/images/aircraft.png')}}" class="img-fluid">

        </div>
      </div>
    </div>
  </div>

  <div class="why-cntr">
    <div class="" id="headingFive">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
           <i class="fa fa-plus" aria-hidden="true"></i> <span>05</span>
        </button>
      </h2>
    </div>
  <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
    <div class="row pt-5 pb-5">
       <div class="col-md-6 text-right">
               <img src="{{asset('assets/saakochi/images/classroom.png')}}" class="img-fluid">

       </div>
       <div class="col-md-6">
           <div class="num">05</div>
           <div class="clearfix"></div>
           <p>
            we are imparting education through Digital Smart & Air-Conditioned Classes which facilitates better comprehension, learning and reiterate the fact that technology is now an integral part of the teaching system.
           </p>
       </div>
     </div>
   </div>
 </div>

 <div class="why-cntr">
        <div class="" id="headingSix">
          <h2 class="mb-0">
            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
               <i class="fa fa-plus" aria-hidden="true"></i> <span>06</span>
            </button>
          </h2>
        </div>
      <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
        <div class="row pt-5 pb-5">
        <div class="col-md-6">
        <div class="num num2">06</div>
        <div class="clearfix"></div>
        <p class="text-allign-right">Institute is filled with various workshops and working models related to aircraft systems i.e Airframe Shop, Engine Shop , Precision Shop, Welding Shop, Machine Shop, Fitting Shop, Instrument Shop, Radio Shop, Electrical Shop, etc.. for uncompromised training to AME students.</p>
        </div>
        <div class="col-md-6 text-left">
        <img src="{{asset('assets/saakochi/images/workshop.png')}}" class="img-fluid">
         </div>
         </div>
       </div>
     </div>

     <div class="why-cntr">
            <div class="" id="headingSeven">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                   <i class="fa fa-plus" aria-hidden="true"></i> <span>07</span>
                </button>
              </h2>
            </div>
          <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
            <div class="row pt-5 pb-5">
               <div class="col-md-6 text-right">
                       <img src="{{asset('assets/saakochi/images/edu.png')}}" class="img-fluid">
               </div>
               <div class="col-md-6">
                   <div class="num">07</div>
                   <div class="clearfix"></div>
                   <p>The educational scenario in Kerala is far advanced than other states of India. It is ranked as one of the most literate states. The Kerala model of development owes it attributed success to the achievements in the area of education and health.</p>
               </div>
             </div>
           </div>
         </div>

         <div class="why-cntr">
                <div class="" id="headingEight">
                  <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                       <i class="fa fa-plus" aria-hidden="true"></i><span>08</span>
                    </button>
                  </h2>
                </div>
              <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample">
                <div class="row pt-5 pb-5">
                <div class="col-md-6">
                <div class="num num2">08</div>
                <div class="clearfix"></div>
                <p class="text-allign-right">Kerala is India's first tourism Super Brand, It is one of the most popular tourist destinations in the country. Named as one of the ten paradises of the world by National Geographic Traveler. Kerala God's Own Country, became a global super brand. Kerala is regarded as one of the destinations with the highest brand recall. Kerala attracted 660,000+ foreign tourist arrivals in a year.</p>
                </div>
                <div class="col-md-6 text-left">
                <img src="{{asset('assets/saakochi/images/kerala.png')}}" class="img-fluid">
                   </div>
                 </div>
               </div>
             </div>

             <div class="why-cntr">
                    <div class="" id="headingNine">
                      <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                           <i class="fa fa-plus" aria-hidden="true"></i><span>09</span>
                        </button>
                      </h2>
                    </div>
                  <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionExample">
                    <div class="row pt-5 pb-5">
                       <div class="col-md-6 text-right">
                               <img src="{{asset('assets/saakochi/images/cochin_airport.png')}}" class="img-fluid">
                       </div>
                       <div class="col-md-6">
                           <div class="num">09</div>
                           <div class="clearfix"></div>
                           <p>Kerala state has 4 operational International airports as of 2018 and is the only Indian state having 4 international airports. Kochi, Trivandrum, Kozhikode, and Tiruchirappalli hosted more international passengers than those travelling within the country.</p>
                       </div>
                     </div>
                   </div>
                 </div>

                 <div class="why-cntr">
                        <div class="" id="headingTen">
                          <h2 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                               <i class="fa fa-plus" aria-hidden="true"></i><span>10</span>
                            </button>
                          </h2>
                        </div>
                      <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordionExample">
                        <div class="row pt-5 pb-5">
                                <div class="col-md-6">
                                        <div class="num num2">10</div>
                                        <div class="clearfix"></div>
                                        <p class="text-allign-right">Our students get better placement opportunities in domestic operators like Air-India, Jet Airways, Indigo, Spice-jet, Air Asia etc & As Kerala its people are well connected with international destinations like Gulf countries, Middle east countries etc. so our students easily get opportunity work with international aviation players like Oman Air, Qatar Airways, Emirates airlines, Lufthansa Airlines, British Airways, Fly Dubai etc.</p>
                                    </div>
                           <div class="col-md-6 text-left">
                                   <img src="{{asset('assets/saakochi/images/over-img-3.jpg')}}" class="img-fluid">
                           </div>
                         </div>
                       </div>
                     </div>

</div>
</div>
</div>
</div>
</div>

<div class="main-sec approval">
	<div class="container">

		<div class="row ">
			<div class="col-lg-12 col-md-12 col-sm-12 col-12">
				<h1>Approval & Recognition</h1>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

				<div class="slider approval-slide">

				    <div>
				     <div class="approval-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/cert.jpg')}}" class="img-fluid">

				     </div>
				    </div>
				    <div>
				     <div class="approval-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/cert.jpg')}}" class="img-fluid">

				     </div>
				    </div>
				    <div>
				     <div class="approval-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/cert.jpg')}}" class="img-fluid">

				     </div>
				    </div>
				    <div>
				     <div class="approval-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/cert.jpg')}}" class="img-fluid">

				     </div>
				    </div>
				    <div>
				     <div class="approval-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/cert.jpg')}}" class="img-fluid">

				     </div>
				    </div>
				  </div>
			</div>
		</div>

	</div>
</div>

<div class="main-sec enrolled">
	<div class="container">

		<div class="row ">
			<div class="col-lg-12 col-md-12 col-sm-12 col-12">
				<h1>Enrolled scholars</h1>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
<ul class="nav nav-pills justify-content-center" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">AME<br/><span>(B 35)</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">AME<br/><span>(B 36)</span></a>
  </li>
  <li class="nav-item">
        <a class="nav-link" id="pills-batch" data-toggle="pill" href="#pills-home-batch" role="tab" aria-controls="pills-home" aria-selected="false">AME<br/><span>(B 37)</span></a>
      </li>
</ul>
<div class="tab-content" id="pills-tabContent">


  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

        <div class="slider approval-slide">

				    <div>
				     <div class="enrolled-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/enrolled_scholars/AMIT_BABAR.png')}}" class="img-fluid">

				    	<div class="enrolled-det">
				    	<span>3508</span>
				    	<p>AMIT BABAR</p>
				    	</div>
				    	<div class="clearfix"></div>
				     </div>
				    </div>


				    			    <div>
				     <div class="enrolled-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/enrolled_scholars/ANSON_SIBY.png')}}" class="img-fluid">

				    	<div class="enrolled-det">
				    	<span>5365</span>
				    	<p>ANSON SIBY</p>
				    	</div>
				    	<div class="clearfix"></div>
				     </div>
				    </div>

			    <div>
				     <div class="enrolled-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/enrolled_scholars/ANUGRAH_KRISHNA.png')}}" class="img-fluid">

				    	<div class="enrolled-det">
				    	<span>05</span>
				    	<p>ANUGRAH KRISHNA</p>
				    	</div>
				    	<div class="clearfix"></div>
				     </div>
				    </div>

			    <div>
				     <div class="enrolled-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/enrolled_scholars/SULTHAN_K.png')}}" class="img-fluid">

				    	<div class="enrolled-det">
				    	<span>25</span>
				    	<p>SULTHAN K.T</p>
				    	</div>
				    	<div class="clearfix"></div>
				     </div>
				    </div>

			    <div>
				     <div class="enrolled-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/enrolled_scholars/FUHAD_SENIN.png')}}" class="img-fluid">

				    	<div class="enrolled-det">
				    	<span>3511</span>
				    	<p>FUHAD SENIN A</p>
				    	</div>
				    	<div class="clearfix"></div>
				     </div>
				    </div>
				  </div>
                </div>

  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">

<div class="slider approval-slide">

				    <div>
				     <div class="enrolled-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/enrolled_scholars/ANKIT_SAHU_36.png')}}" class="img-fluid">

				    	<div class="enrolled-det">
				    	<span>3602</span>
				    	<p>ANKIT SAHU</p>
				    	</div>
				    	<div class="clearfix"></div>
				     </div>
				    </div>


				    			    <div>
				     <div class="enrolled-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/enrolled_scholars/MUKESH_CHANDEL-36.png')}}" class="img-fluid">

				    	<div class="enrolled-det">
				    	<span>16</span>
				    	<p>MUKESH CHANDEL</p>
				    	</div>
				    	<div class="clearfix"></div>
				     </div>
				    </div>



			    <div>
				     <div class="enrolled-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/enrolled_scholars/SUMIT_KUMAR_36.png')}}" class="img-fluid">

				    	<div class="enrolled-det">
				    	<span>24</span>
				    	<p>SUMIT KUMAR</p>
				    	</div>
				    	<div class="clearfix"></div>
				     </div>
				    </div>



			    <div>
				     <div class="enrolled-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/enrolled_scholars/SAYAK_PURKASTHA-36.png')}}" class="img-fluid">

				    	<div class="enrolled-det">
				    	<span>3627</span>
				    	<p>SAYAK PURKASTHA</p>
				    	</div>
				    	<div class="clearfix"></div>
				     </div>
				    </div>

			    <div>
				     <div class="enrolled-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/enrolled_scholars/SATHYA_PRIYA_36.png')}}" class="img-fluid">

				    	<div class="enrolled-det">
				    	<span>3609</span>
				    	<p>SATHYA PRIYA</p>
				    	</div>
				    	<div class="clearfix"></div>
				     </div>
				    </div>




				  </div>


  </div>
  <div class="tab-pane fade" id="pills-home-batch" role="tabpanel" aria-labelledby="pills-home-tab">

    <div class="slider approval-slide">

                        <div>
                         <div class="enrolled-slide-cntr">
                                <img src="{{asset('assets/saakochi/images/enrolled_scholars/AADHIL_ABUBACKER_37.png')}}" class="img-fluid">

                            <div class="enrolled-det">
                            <span>10</span>
                            <p>AADHIL ABUBACKER</p>
                            </div>
                            <div class="clearfix"></div>
                         </div>
                        </div>


                        <div>
                         <div class="enrolled-slide-cntr">
                                <img src="{{asset('assets/saakochi/images/enrolled_scholars/MANISHA_YADEV_37.png')}}" class="img-fluid">

                            <div class="enrolled-det">
                            <span>3708</span>
                            <p>MANISHA YADEV</p>
                            </div>
                            <div class="clearfix"></div>
                         </div>
                        </div>

                    <div>
                         <div class="enrolled-slide-cntr">
                                <img src="{{asset('assets/saakochi/images/enrolled_scholars/RAIHAN_MUZAFFAR_37.png')}}" class="img-fluid">

                            <div class="enrolled-det">
                            <span>3616</span>
                            <p>RAIHAN MUZAFFAR</p>
                            </div>
                            <div class="clearfix"></div>
                         </div>
                        </div>

                    <div>
                         <div class="enrolled-slide-cntr">
                                <img src="{{asset('assets/saakochi/images/enrolled_scholars/SHIVAM_SINGH_37.png')}}" class="img-fluid">

                            <div class="enrolled-det">
                            <span>3707</span>
                            <p>SHIVAM SINGH</p>
                            </div>
                            <div class="clearfix"></div>
                         </div>
                        </div>

                    <div>
                         <div class="enrolled-slide-cntr">
                                <img src="{{asset('assets/saakochi/images/enrolled_scholars/RUMANA_SHAIKH_37.png')}}" class="img-fluid">

                            <div class="enrolled-det">
                            <span>3302</span>
                            <p>RUMANA SHAIKH</p>
                            </div>
                            <div class="clearfix"></div>
                         </div>
                        </div>


                      </div>
      </div>
</div>
			</div>
		</div>

	</div>
</div>






<div class="main-sec mentor">
	<div class="container">

		<div class="row ">
			<div class="col-lg-12 col-md-12 col-sm-12 col-12">
				<h1>OUR MENTORS</h1>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>


				<div class="slider approval-slide mentor">



			    <div>
				     <div class="mentor-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/mentor.jpg')}}" class="img-fluid">

				    	<div class="mentor-det">
				    	<span>Mr. Priyesh Mishra</span>
				    	<p>AME BAMEC IN HA,JE<br/>
							Training Manager, B1.1<br/>
							11 YRS Experience
						</p>
				    	<div class="clearfix"></div>
				    	</div>
				    	<div class="clearfix"></div>
				     </div>
				   </div>

				     <div>
				     <div class="mentor-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/mentor.jpg')}}" class="img-fluid">

				    	<div class="mentor-det">
				    	<span>Mr. Priyesh Mishra</span>
				    	<p>AME BAMEC IN HA,JE<br/>
							Training Manager, B1.1<br/>
							11 YRS Experience
						</p>
				    	<div class="clearfix"></div>
				    	</div>
				    	<div class="clearfix"></div>
				     </div>
				   </div>


				     <div>
				     <div class="mentor-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/mentor.jpg')}}" class="img-fluid">

				    	<div class="mentor-det">
				    	<span>Mr. Priyesh Mishra</span>
				    	<p>AME BAMEC IN HA,JE<br/>
							Training Manager, B1.1<br/>
							11 YRS Experience
						</p>
				    	<div class="clearfix"></div>
				    	</div>
				    	<div class="clearfix"></div>
				     </div>
				   </div>


				     <div>
				     <div class="mentor-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/mentor.jpg')}}" class="img-fluid">

				    	<div class="mentor-det">
				    	<span>Mr. Priyesh Mishra</span>
				    	<p>AME BAMEC IN HA,JE<br/>
							Training Manager, B1.1<br/>
							11 YRS Experience
						</p>
				    	<div class="clearfix"></div>
				    	</div>
				    	<div class="clearfix"></div>
				     </div>
				   </div>

				     <div>
				     <div class="mentor-slide-cntr">
                            <img src="{{asset('assets/saakochi/images/mentor.jpg')}}" class="img-fluid">

				    	<div class="mentor-det">
				    	<span>Mr. Priyesh Mishra</span>
				    	<p>AME BAMEC IN HA,JE<br/>
							Training Manager, B1.1<br/>
							11 YRS Experience
						</p>
				    	<div class="clearfix"></div>
				    	</div>
				    	<div class="clearfix"></div>
				     </div>
				   </div>
				  </div>
			</div>
		</div>
	</div>
</div>
@endsection



