@extends('layouts.inner')
@section('content')

    <div class="container-fluid slider_cc p-0">
      <div class="banner_slider_inner">
        <div class="banner_slider">
          <div class="banner_slider_top">
            <div class="banner_slider">
              <div class=" slide slide--1" >
                <img src="{{ asset('assets/saakochi/images/course-structure.jpg')}}" class="img-fluid">
              </div>
              <div class="overlay">
              </div>
              <div class="overlay-content inner-page">
                  <h2>{{$page->name}}</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="main-sec abou-sec-1">
        <div class="container">
            {!! $page->content !!}
        </div>
    </div>

@endsection
