@extends('layouts.inner')
@section('content')

<div class="container-fluid slider_cc p-0">

  <div class="banner_slider_inner">
    <div class="banner_slider">
      <div class="banner_slider_top">
        <div class="banner_slider">
          <div class=" slide slide--1" >
          	<img src="images/job.jpg" class="img-fluid">
          </div>
          <div class="overlay">
          </div>
          <div class="overlay-content inner-page">
              
              <h2>Job Prospectus</h2>
        
          </div>

        </div>
      </div>
    </div>
   
 
  </div>
</div>

<div class="main-sec abou-sec-1">
	<div class="container">
<div class="row ">

  <div class="col-lg-6 col-md-6 ">
    <h1>Job Prospectus</h1>
    <p>After having aircraft maintenance engineer license, career opportunities are available with important organisations for Aircraft Maintenance EngineersIndian Airlines, Air India, Air India Chartered Services, Alliance Air, Pawan Hans Limited, Flying Training Institutes of all states in India, A.M.E. Training Organisations, Agro Aviation, Public Sector (SAIL/ONGC), Big business having their own aviation wings, State Govt. Aviation Department, National Airport Authority, National Remote Sensing Agency, Director General of Civil Aviation, Govt. of India, Coast Guard (Central Govt.), Jet Airways, Sahara Air Lines, Deccan Aviation Pvt. Limited, U.B. Airways, Jagson Air Lines, MESCO Air Lines, Private Air Taxi Operators, H.A.L. (Hindustan Aeronautics Limited), N.A.L.(National Aeronautical Laboratory), T.A.A.L. (Taneja Aerospace & Aviation Limited), Air Deccan, Kingfisher Airlines, Spice Air and many more.</p>
    <p>
  </div>
	
	<div class="col-lg-6 col-md-6 ">
		
    <div class="content" style="height: 540px;">

@for($i=1; $i<=50; $i++)
  @if($i == 0 || (($i-1)%3)==0)
    <div class="row m-0 mt-4 mb-4">
  @endif
    <div class="col-lg-4 col-md-4 "> <img src="{{asset('assets/saakochi/saakochi_images')}}/Job_Prospectus{{$i}}.jpg" class="img-fluid w-100"> </div>
  @if($i%3 == 0 || $i == 50)
    </div>
  @endif
@endfor
	
  </div>
	</div>
	
</div>
</div>
</div>















@endsection