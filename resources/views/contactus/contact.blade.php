@extends('layouts.inner')


@section('content')
<div class="container-fluid slider_cc p-0">
  <div class="banner_slider_inner">
    <div class="banner_slider">
      <div class="banner_slider_top">
        <div class="banner_slider">
          <div class=" slide slide--1 contact-hd" >
          	
          </div>
          <div class="overlay">
          </div>
          <div class="overlay-content inner-page contact">            
              <h2>A wonderful serenity has taken possession
of my entire soul, like these. </h2> 
<p>470-A/9, Near SAJ Hotel & Resorts Opposite,
Cochin International Airport, Nedumbassery, Cochin (Kerla), India.</p>   
 <p>info@saakochi.com</p> 
 <p>+91-9594-929-936 (Hindi/English), +91-6362-456-159 (Hindi/English)<br/>
+91-7592-916-000 (Malyalam/English), +91-7356-714-416 (Malyalam/English)</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="main-sec abou-sec-1">
	<div class="container">



<div class="row ">
	
	<div class="col-lg-12 col-md-12">
    <div class="contact-cntr">

      <div class="row ">
        <div class="col-lg-6 col-md-6">
          
          <div class="embed-responsive embed-responsive-16by9 h-100">

  <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12113.242549804707!2d76.39142061327865!3d10.16170516883326!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x94fd78aeb4f55ae7!2sSHA-SHIB%20AVIATION%20ACADEMY!5e0!3m2!1sen!2sin!4v1575107130331!5m2!1sen!2sin" width="600" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>
        </div>
        <div class="col-lg-6 col-md-6">

          <div class="contact-form">
            <h1>LEAVE US YOUR INFO</h1>
            <p>AND WE WILL GET BACK TO YOU.</p>
            
               @if($message=Session::get("success"))
            <p class="alert alert-success" role="alert">{{$message}}</p>
              @endif
            <form action="{{url('create-contact')}}" id="ContactFrm" method="POST" class="form-horizontal style-form" >
              @csrf
            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                <input type="text"  name="name" id="name" class="form-control" placeholder="Name">
              <div>  {!! $errors->first('name', '<p  class="alert alert-danger">:message</p>') !!}</div>
            </div>
            <!--  @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <i><p style="color:red; font-style: normal;">{{ $message }}</p></i>
                                    </span>
                                @enderror   -->
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                 {!! $errors->first('email', '<p class="alert alert-danger">:message</p>') !!}
            </div>
            <!--  @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <i><p style="color:red; font-style: normal;">{{ $message }}</p></i>
                                    </span>
                                @enderror   -->
            <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
                <input type="number" name="phone" id="phone" class="form-control" placeholder="Phone">
                 {!! $errors->first('phone', '<p class="alert alert-danger">:message</p>') !!}
            </div>
             <!-- @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <i><p style="color:red; font-style: normal;">{{ $message }}</p></i>
                                    </span>
                                @enderror   -->
            <div class="form-group {{ $errors->has('subject') ? 'has-error' : ''}}">
                <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject">
                  {!! $errors->first('subject', '<p class="alert alert-danger">:message</p>') !!}
                 <!-- @error('subject')
                                    <span class="invalid-feedback" role="alert">
                                        <i><p style="color:red; font-style: normal;">{{ $message }}</p></i>
                                    </span>
                                @enderror   -->
            </div>
            <div class="form-group {{ $errors->has('messages') ? 'has-error' : ''}}">
                 <textarea class="form-control" name="messages" id="messages" placeholder="messages"  rows="3"></textarea>
                   {!! $errors->first('messages', '<p class="alert alert-danger">:message</p>') !!}
             </div>

            <!--  @error('messages')
                                    <span class="invalid-feedback" role="alert">
                                        <i><p style="color:red; font-style: normal;">{{ $message }}</p></i>
                                    </span>
                                @enderror   -->
            
            <button class="btn" type="submit">submit</button>
          </form>
                    


          </div>




        </div>
      </div>
    

</div>
</div>
</div>
</div>
</div>

    </div>
 
	</div>
  @endsection
  @section('bottom')
  <script type="text/javascript">
    
    var validator = $('#ContactFrm').validate(
    {
          rules:{
              name: 
              {
                    required: true,
              },
              email: 
              {
                    email: true,
                    required:true, 
              },
              phone: 
              {
                    required: true,
                    number: true,
                    minlength:10, 
                    maxlength:16,
              },
                
    },
          messages:
          {
              name: 
              {
                required: "Name cannot be blank",
              },
              email: 
              {
                required: "Email cannot be blank",
              },
              phone: 
              {
                required: "Phone number cannot be blank",
              },
                
          }
              
});

</script>
    @endsection


