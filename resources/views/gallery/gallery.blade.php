
@extends('layouts.inner')
@section('content')

<div class="container-fluid slider_cc p-0">

  <div class="banner_slider_inner">
    <div class="banner_slider">
      <div class="banner_slider_top">
        <div class="banner_slider">
          <div class=" slide slide--1" >
            <img src="{{asset('assets/saakochi/images/gall-banner.jpg')}}" class="img-fluid">
          </div>
          <div class="overlay">
          </div>
          <div class="overlay-content inner-page">
              
              <h2>Gallery</h2>
        
          </div>

        </div>
      </div>
    </div>
   
 
  </div>
</div>





<div class="main-sec all-page">
  <div class="container">




  <div class="row">
        <div class="col-lg-12">
               <div class="toolbar">
            <button class="btn fil-cat current" data-rel="all">All </button>
            <button class="btn fil-cat" data-rel="image">Image </button>
            <button class="btn fil-cat" data-rel="video"> Video</button>
          </div>
               <div style="clear:both;"></div>
             </div>
      </div>
         
        <div id="portfolio" style="opacity: 1;">



          <div class="row">

            <div class="col-xl-3 col-lg-3 col-md-4 tile  all image scale-anm" style="display: block;">
               <a class="example-image-link" href="{{asset('assets/saakochi/images/aircraft.jpg')}}" data-lightbox="example-set" data-title="Click the right half of the image to move forward."> 
                <img src="{{asset('assets/saakochi/images/aircraft.jpg')}}" width="100%" class="img-fluid example-image">
               </a>
                  
            </div>

            <div class="col-xl-3 col-lg-3 col-md-4 tile  all video scale-anm" style="display: block;">  
                <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/lYcOlKNSrUI" allowfullscreen></iframe>
              </div>
             
            </div>

            <div class="col-xl-3 col-lg-3 col-md-4 tile  all image scale-anm" style="display: block;">
                    <a class="example-image-link" href="{{asset('assets/saakochi/images/aircraft.jpg')}}" data-lightbox="example-set" data-title="Click the right half of the image to move forward."> 
                <img src="{{asset('assets/saakochi/images/aircraft.jpg')}}" width="100%" class="img-fluid example-image">
               </a>
            </div>

            <div class="col-xl-3 col-lg-3 col-md-4 tile  all image scale-anm" style="display: block;">
                     <a class="example-image-link" href="{{asset('assets/saakochi/images/aircraft.jpg')}}" data-lightbox="example-set" data-title="Click the right half of the image to move forward."> 
                <img src="{{asset('assets/saakochi/images/aircraft.jpg')}}" width="100%" class="img-fluid example-image">
               </a>
            </div>
           
          
                 
                 
                 
                 
                 
          </div>
             </div>
        <div style="clear:both;"></div>
    



</div>
</div>

@endsection