<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spiderworks\MiniWeb\Models\FrontendPage;

class AccademicController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $page = FrontendPage::where('slug', 'accademic.index')->first();
        return view('accademic.index')->with('page', $page);
    }
    public function syllabus()
    {
        $page = FrontendPage::where('slug', 'accademic.syllabus')->first();
        return view('accademic.syllabus')->with('page', $page);
    }
    
}
