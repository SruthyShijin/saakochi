<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spiderworks\MiniWeb\Controllers\BaseController;
use DB, URL;
use Spiderworks\MiniWeb\Models\MediaLibrary as Media;
use Illuminate\Support\Facades\Auth;

class PluginController extends BaseController
{

    public function select2_pages(Request $r){
        $items = DB::table('pages')->where('name', 'like', $r->q.'%')->orderBy('name')
            ->get();
        $json = [];
        foreach($items as $c){
            $json[] = ['id'=>$c->id, 'text'=>$c->name];
        }
        return \Response::json($json);
    }

    public function select2_locations(Request $r){
        $items = DB::table('locations')->where('name', 'like', $r->q.'%')->orderBy('name')
            ->get();
        $json = [];
        foreach($items as $c){
            $json[] = ['id'=>$c->id, 'text'=>$c->name];
        }
        return \Response::json($json);
    }

    public function select2_city(Request $r){
        $items = DB::table('cities')->where('name', 'like', $r->q.'%')->orderBy('name')
            ->get();
        $json = [];
        foreach($items as $c){
            $json[] = ['id'=>$c->id, 'text'=>$c->name];
        }
        return \Response::json($json);
    }

    public function select2_locations_with_slug(Request $r){
        $items = DB::table('locations')->where('name', 'like', $r->q.'%')->orderBy('name')
            ->get();
        $json = [];
        foreach($items as $c){
            $json[] = ['id'=>$c->slug, 'text'=>$c->name];
        }
        return \Response::json($json);
    }

    public function select2_districts(Request $r){
        $items = DB::table('district')->where('district_name', 'like', $r->q.'%')->orderBy('district_name')
            ->get();
        $json = [];
        foreach($items as $c){
            $json[] = ['id'=>$c->district_id, 'text'=>$c->district_name];
        }
        return \Response::json($json);
    }

    public function select2_models(Request $r){
        $items = DB::table('model')->where('model_name', 'like', $r->q.'%')->orderBy('model_name')
            ->get();
        $json = [];
        foreach($items as $c){
            $json[] = ['id'=>$c->model_id, 'text'=>$c->model_name];
        }
        return \Response::json($json);
    }

    public function select2_variants(Request $r, $model=null){
        $items = DB::table('variant')->where('variant_name', 'like', $r->q.'%');
        if($model)
            $items->where('model', $model);
        $items = $items->orderBy('variant_name')->get();
        $json = [];
        foreach($items as $c){
            $json[] = ['id'=>$c->variant_id, 'text'=>$c->variant_name];
        }
        return \Response::json($json);
    }

    public function select2_dealers(Request $r){
        $items = DB::table('dealership')->where('dealership_name', 'like', $r->q.'%')->orderBy('dealership_name')
            ->get();
        $json = [];
        foreach($items as $c){
            $json[] = ['id'=>$c->dealership_id, 'text'=>$c->dealership_name];
        }
        return \Response::json($json);
    }

    public function select2_cars(Request $r){
        $items = DB::table('car')->select('car.car_id AS id', 'car.thumb', 'variant.variant_name')->join('variant', 'car.variant', '=', 'variant.variant_id')->where('variant.variant_name', 'like', $r->q.'%')->orWhere('car.thumb', 'like', $r->q.'%')->orderBy('variant.variant_name')
            ->take(10)->get();
        $json = [];
        foreach($items as $c){
            $json[] = ['id'=>$c->id, 'text'=>$c->variant_name.'('.$c->thumb.')'];
        }
        return \Response::json($json);
    }

    public function select2_full_search(Request $r)
    {
        $locations = DB::table('locations')->select(DB::raw("CONCAT('location', '-', id) AS id"), 'name')->where('name', 'like', '%'.$r->q.'%')->where('status', '1');
        $models = DB::table('model')->select(DB::raw("CONCAT('model', '-', model_id) AS id"), 'model_name AS name')->where('model_name', 'like', '%'.$r->q.'%')->where('status', 'Y');
        $variants = DB::table('variant')->select(DB::raw("CONCAT('variant', '-', variant_id) AS id"), 'variant_name AS name')->where('variant_name', 'like', '%'.$r->q.'%')->where('status', 'Y');
        $items = DB::table('variant_locations')->select(DB::raw("CONCAT('variant_location', '-', variant_locations.id) AS id"), DB::raw("CONCAT(variant.variant_name, ' in ', locations.name) AS name"))->join('variant', 'variant.variant_id', '=', 'variant_locations.variant_id')->join('locations', 'locations.id', '=', 'variant_locations.location_id')->where('variant_locations.status', 'Y')->having('name', 'like', '%'.$r->q.'%')->union($locations)->union($models)->union($variants)->orderByRaw("CASE WHEN name LIKE '".$r->q."%' THEN 1 WHEN name LIKE '%".$r->q."' THEN 3 ELSE 2 END, name ASC")->take(10)->get();

        $json = [];
        foreach($items as $c){
            $json[] = ['id'=>$c->id, 'text'=>$c->name];
        }
        return \Response::json($json);
    }

    public function unique_user(Request $r)
    {
         $id = $r->id;
         $email = $r->email;
         
         $where = "email='".$email."'";
         if($id)
            $where .= " AND id != ".decrypt($id);
         $user = DB::table('users')
                    ->whereRaw($where)
                    ->get();
         
         if (count($user)>0) {  
             echo "false";
         } else {  
             echo "true";
         }
    }
    
    public function unique_location(Request $r)
    {
         $id = $r->id;
         $email = $r->slug;
         
         $where = "slug='".$slug."'";
         if($id)
            $where .= " AND id != ".decrypt($id);
         $user = DB::table('locations')
                    ->whereRaw($where)
                    ->get();
         
         if (count($user)>0) {  
             echo "false";
         } else {  
             echo "true";
         }
    }
    
    public function unique_model_location(Request $r)
    {
         $id = $r->id;
         $email = $r->slug;
         
         $where = "slug='".$slug."'";
         if($id)
            $where .= " AND id != ".decrypt($id);
         $result = DB::table('model_locations')
                    ->whereRaw($where)
                    ->get();
         
         if (count($result)>0) {  
             echo "false";
         } else {  
             echo "true";
         }
    }
    
    public function unique_variant_location(Request $r)
    {
         $id = $r->id;
         $slug = $r->slug;
         
         $where = "slug='".$slug."'";
         if($id)
            $where .= " AND id != ".decrypt($id);
         $result = DB::table('variant_locations')
                    ->whereRaw($where)
                    ->get();
         
         if (count($result)>0) {  
             echo "false";
         } else {  
             echo "true";
         }
    }
     public function unique_slug(Request $r)
    {
         $id = $r->id;
         $slug = $r->slug;

         $where = "slug='".$slug."'";
         if($id)
            $where .= " AND id != ".decrypt($id);
                 
         $meta_details = DB::table('meta_details')
                    ->whereRaw($where)
                    ->get();
         
         if (count($meta_details)>0) 
         {  
             echo "false";
         } 
         else 
         {  
             echo "true";
         }
    }
}
