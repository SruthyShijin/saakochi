<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spiderworks\MiniWeb\Models\FrontendPage;

class AdmissionController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */



    public function asked_question()
    {
        $page = FrontendPage::where('slug', 'admission.asked_question')->first();
        return view('admission.faq')->with('page', $page);
    }
    public function index()
    {
        $page = FrontendPage::where('slug', 'admission.index')->first();
        return view('admission.index')->with('page', $page);
    }


     public function notfound()
    {
       
        return view('admission.notfound');
    }
    public function apply()
    {
       
        return view('apply.enquiry');
    }
    public function broacher()
    {
       
        return view('admission.e-broacher');
    }
}
