<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spiderworks\MiniWeb\Models\FrontendPage;

class PlacementController extends Controller
{

    public function job_prospectus()
    {
        $page = FrontendPage::where('slug', 'placement.job_prospectus')->first();
        return view('placement.job_prospectus')->with('page', $page);
    }

}
