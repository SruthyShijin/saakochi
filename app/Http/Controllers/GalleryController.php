<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spiderworks\MiniWeb\Models\FrontendPage;

class GalleryController extends Controller
{
    public function images()
    {
        $page = FrontendPage::where('slug', 'gallery')->first();
        return view('gallery.gallery');
    }

   
}
