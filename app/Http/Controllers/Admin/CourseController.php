<?php 

namespace App\Http\Controllers\Admin;
use Spiderworks\MiniWeb\Controllers\BaseController;
use App\Traits\ResourceTrait;
use View, Redirect, DB;
use App\Models\Course;
use App\Models\CourseCategory;
use Illuminate\Http\Request as HttpRequest;

class CourseController extends BaseController
{
     use ResourceTrait;
     protected $model_path;
    
    public function __construct()
    {
        parent::__construct();
       

        $this->model = new Course;
        $this->route = 'admin.course';
        $this->views = 'admin.course';
        $this->model_path = 'App\Models\Course';
        
        $this->resourceConstruct();

    }

    protected function getCollection() {
        return $this->model->select('id', 'course_name', 'top_description','bottom_description', 'created_at', 'updated_at');
        
    }

    protected function setDTData($collection) 
    {
        $route = $this->route;
        return $this->initDTData($collection)
            
            ->rawColumns(['action_edit', 'action_delete']);
    }
    public function create()
    {
        $category=CourseCategory::get();
        return view($this->views . '.form')->with('obj',$this->model)->with('category',$category);
    }
    public function edit($id) 
    {
        $id = decrypt($id);
         $category=CourseCategory::get();
        if($obj = $this->model->find($id))
        {
            return view($this->views . '.form')->with('obj', $obj)->with('category',$category);
        } 
        else 
        {
            return $this->redirect('notfound');
        }
    }


    
    }

   


