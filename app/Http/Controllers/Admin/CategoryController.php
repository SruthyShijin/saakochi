<?php

namespace App\Http\Controllers\Admin;
use Spiderworks\MiniWeb\Controllers\BaseController;
use Spiderworks\MiniWeb\Traits\ResourceTrait;
use App\User, Request, View, Redirect, DB, Datatables, Sentinel, Mail, Validator, Image;
use Activation as Act;
use App\Models\Course;
use App\Models\CourseCategory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request as HttpRequest;

class CategoryController extends BaseController
{
    use ResourceTrait;

    public function __construct()
    {
        parent::__construct();

        $this->model = new Category;
        $this->route .= '.category';
        $this->views .= '.categories';

        $this->resourceConstruct();

    }
    protected function getCollection() {
        return $this->model->select('id', 'slug', 'name', 'browser_title', 'meta_keywords', 'status');
    }

    protected function setDTData($collection) {
        $route = $this->route;
        return $this->initDTData($collection)
            ->editColumn('status', function($obj) use($route) { 
                
            })
            ->rawColumns(['action_edit', 'action_delete', 'status']);
    }

}
