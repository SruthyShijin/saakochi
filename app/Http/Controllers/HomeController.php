<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\contact;
use Spiderworks\MiniWeb\Models\Page;

//use Illuminate\Http\Request;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('index');
    }

    public function page($slug)
    {
        $page = Page::where('slug', $slug)->first();
        if($page)
            return view('page')->with('page', $page);
        else
            abort(404);
    }

      public function contact()
      {
        return view('contactus.contact');
      }
      public function createcontact(Request $request)
      {
        $this->validate($request,[
        'name'=>'required',
        'email'=>'required|unique:contacts',
        'phone'=>'required|max:16|min:10',
         
       ]);
        
        $contacts=new contact;
        $contacts->name=request('name');
        $contacts->email=request('email');
        $contacts->phone=request('phone');
        $contacts->subject=request('subject');
        $contacts->messages=request('messages');
        $contacts->save();

        return redirect()->back()->with("success","Message added successfully");
        


      }
}
