<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spiderworks\MiniWeb\Models\FrontendPage;

class AboutController extends Controller
{
    public function overview()
    {
        $page = FrontendPage::where('slug', 'about.overview')->first();
        return view('about.overview');
    }

    public function sha_shib_group_of_institutions()
    {
        $page = FrontendPage::where('slug', 'about.about_sha_shib_group_of_institutes')->first();
        return view('about.about_sha_shib_group_of_institutes');
    }

    public function programs_offered()
    {
        $page = FrontendPage::where('slug', 'aircraft-maintenance-engineer')->first();
        return view('about.programs_offered');
    }
    public function hospitality()
    {
        $page = FrontendPage::where('slug', 'airhostess-hospitality-training')->first();
        return view('about.airhostess-hospitality-training');
    }
    public function pilot()
    {
        $page = FrontendPage::where('slug', 'pilot-training')->first();
        return view('about.pilot-training');
    }

}
