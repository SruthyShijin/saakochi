<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;

use Spiderworks\MiniWeb\Models\MenuItem;
class Helper
{
    

    
    public static function menu_tree($menu_id, $parent)
    {
        $items = MenuItem::where('menu_id', $menu_id)->where('parent_id', $parent)->orderBy('menu_order', 'ASC')->get();
        if($items)
        {
            foreach ($items as $key => $item) {
                if($item->menu_type == 'custom_link')
                    $item->url = $item->url;
                elseif ($item->menu_type == 'page_link')
                    $item->url = url($item->linkable->slug);
                elseif($item->menu_type == 'frontpage_links')
                    $item->url = route($item->linkable->slug);
                elseif($item->menu_type == 'category_link')
                    $item->url = $item->linkable->slug;

                $check_children = MenuItem::where('parent_id', $item->id)->count();
                if($check_children>0)
                {
                    $item['children'] = static::menu_tree($menu_id, $item->id);
                }
            }
        }
        return $items;
    }

   
}