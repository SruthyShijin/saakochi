<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CourseCategory extends Model
{
    protected $table = 'course_category';
    protected $fillable = ['category_name','created_at','updated_at'];
    protected function setRules() {

        $this->val_rules = array(
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }
    public function courses()
    {
        return $this->belongsTo('App\Models\Course');
    }
}
