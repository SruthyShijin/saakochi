<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Spiderworks\MiniWeb\Models\BaseModel;
use Spiderworks\MiniWeb\Traits\ValidationTrait;

class Course extends Model
{
	use ValidationTrait {
        ValidationTrait::validate as private parent_validate;
    }
    
    public function __construct() {
        
        parent::__construct();
        $this->__validationConstruct();
    }
    protected $table = 'courses';
    protected $fillable = ['course_name','category_id','top_description','bottom_description','browser_title','meta_keywords','meta_description','featured_image','thumb_image','created_at','updated_at'];
  
  protected function setRules() {

        $this->val_rules = array(
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }
    public function course_category()
    {
        return $this->belongsTo('App\Models\CourseCategory','category_id');
    }
    public function featured()
    {
    	return $this->belongsTo('Spiderworks\MiniWeb\Models\MediaLibrary', 'featured_image');
    }
    public function thumb()
    {
    	return $this->belongsTo('Spiderworks\MiniWeb\Models\MediaLibrary', 'thumb_image');
    }

    
}
