<?php

namespace App\Widgets;
use Arrilot\Widgets\AbstractWidget;
use Spiderworks\MiniWeb\Models\MenuItem;
use Spiderworks\MiniWeb\Models\Category;
use Spiderworks\MiniWeb\Models\FrontendPage;
use Spiderworks\MiniWeb\Models\Page;
use Helper;
class HeaderMenu extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        $menu_items = Helper::menu_tree(1, 0);
        return view('widgets.header_menu', [
            'config' => $this->config,
            'menu_items' => $menu_items,
        ]);
    }
}
