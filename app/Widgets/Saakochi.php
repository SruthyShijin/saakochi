<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use DB;
class Saakochi extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $menu_items = $this->menu_tree(0);
        return view('widgets.saakochi', [
            'config' => $this->config,
            'menu_items' => $menu_items,
        ]);
    }

    public function menu_tree($parent_id)
    {
        $items = DB::table('admin_pages')->where('parent', $parent_id)->get();
        if($items)
        {
            foreach ($items as $key => $item) {
                $check_children = DB::table('admin_pages')->where('parent', $item->id)->count();
                if($check_children>0)
                {
                    $item->children = $this->menu_tree($item->id);
                }
            }
        }
        return $items;
    }
}
