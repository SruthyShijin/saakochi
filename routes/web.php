<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/accademic', 'AccademicController@index')->name('accademic.index');
Route::get('accademic/syllabus', 'AccademicController@syllabus')->name('accademic.syllabus');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('validation/user', 'PluginController@unique_user')->name('validate.unique_user');
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function(){
	Route::get('/', 'AdminController@login');
    Route::group(['middleware' => ['isAdmin']], function(){
    	Route::get('/dashboard', ['as' => 'admin.dashboard.index', 'uses' => 'AdminController@index' ]);


    	//users
        Route::get('users', 'UserController@index')->name('admin.users.index');
        Route::get('users/create', 'UserController@create')->name('admin.users.create');
        Route::get('users/edit/{id}', 'UserController@edit')->name('admin.users.edit');
        Route::get('users/destroy/{id}', 'UserController@destroy')->name('admin.users.destroy');
        Route::get('users/change-status/{id}', 'UserController@changeStatus')->name('admin.users.change-status');
        Route::post('users/store', 'UserController@store')->name('admin.users.store');
        Route::post('users/update', 'UserController@update')->name('admin.users.update');

        //Course
        Route::get('course', 'CourseController@index')->name('admin.course.index');
        Route::get('course/create', 'CourseController@create')->name('admin.course.create');
        Route::get('course/edit/{id}', 'CourseController@edit')->name('admin.course.edit');
        Route::get('course/destroy/{id}', 'CourseController@destroy')->name('admin.course.destroy');
        //Route::get('users/change-status/{id}', 'UserController@changeStatus')->name('admin.users.change-status');
        Route::post('course/store', 'CourseController@store')->name('admin.course.store');
        Route::post('course/update', 'CourseController@update')->name('admin.course.update');

        //Roles
        Route::get('roles', 'RoleController@index')->name('admin.roles.index');
        Route::get('roles/create', 'RoleController@create')->name('admin.roles.create');
        Route::get('roles/edit/{id}', 'RoleController@edit')->name('admin.roles.edit');
        Route::get('roles/destroy/{id}', 'RoleController@destroy')->name('admin.roles.destroy');
        //Route::get('users/change-status/{id}', 'UserController@changeStatus')->name('admin.users.change-status');
        Route::post('roles/store', 'RoleController@store')->name('admin.roles.store');
        Route::post('roles/update', 'RoleController@update')->name('admin.roles.update');

        });

	});


 Route::get('/overview', 'AboutController@overview')->name('overview');
 Route::get('/about-sha-shib-aviation-academy', 'AboutController@sha_shib_group_of_institutions')->name('about-sha-shib-aviation-academy');
 Route::get('/aircraft-maintenance-engineer', 'AboutController@programs_offered')->name('aircraft-maintenance-engineer');
 Route::get('/airhostess-hospitality-training', 'AboutController@hospitality')->name('airhostess-hospitality-training');
 Route::get('/pilot-training', 'AboutController@pilot')->name('pilot-training');


Route::get('/contact_us','HomeController@contact')->name('contact_us');
Route::post('/create-contact','HomeController@createcontact');
 Route::get('/gallery', 'GalleryController@images')->name('gallery');
 Route::get('/placement', 'PlacementController@job_prospectus')->name('placement.job_prospectus');
 Route::get('/admission', 'AdmissionController@index')->name('admission.index');

 Route::get('admission.asked-question', 'AdmissionController@asked_question')->name('admission.asked_question');


 Route::get('error404', 'AdmissionController@notfound');
 Route::get('not', 'AdmissionController@notfound');
Route::get('error', 'AdmissionController@notfound');
Route::get('practical-training-industries', 'AdmissionController@notfound');

Route::get('required-documents', 'AdmissionController@notfound');
Route::get('our-pearls', 'AdmissionController@notfound');



 Route::get('enquiry', 'AdmissionController@apply')->name('enquiry');
 Route::get('e-broacher', 'AdmissionController@broacher')->name('e-broacher');
 /*Route::get('enquiry', function(){
     return view('enquiry');

 });*/





 Route::get('{slug}', 'HomeController@page')->name('static-page');


